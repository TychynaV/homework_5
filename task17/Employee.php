<?php

namespace Task17;

class Employee extends User
{
    /** @var int  */
    public int $salary;

    /**
     * Employee constructor.
     * @param string $name
     * @param string $surname
     * @param int $salary
     */
    public function __construct(string $name, string $surname, int $salary)
    {
        parent::__construct($name, $surname);
        $this->salary = $salary;
    }
}
