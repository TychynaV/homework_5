<?php

namespace Task17;

class City
{
    /** @var string  */
    public string $name;

    /** @var int  */
    public int $population;

    /**
     * City constructor.
     * @param string $name
     * @param int $population
     */
    public function __construct(string $name, int $population)
    {
        $this->name = $name;
        $this->population = $population;
    }
}
