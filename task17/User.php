<?php

namespace Task17;

class User
{
    /** @var string  */
    public string $name;

    /** @var string  */
    public string $surname;

    /**
     * User constructor.
     * @param string $name
     * @param string $surname
     */
    public function __construct(string $name, string $surname)
    {
        $this->name = $name;
        $this->surname = $surname;
    }
}
