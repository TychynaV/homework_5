<?php

namespace Task40;

abstract class Building
{
    /** @var string  */
    protected string $address;

    /** @var int  */
    protected int $floors;

    /** @var string  */
    protected string $materialWall;

    /** @var int  */
    protected int $countWindows;

    public function __construct(string $address, int $floors, string $materialWall, int $countWindows)
    {
        $this->address = $address;
        $this->floors = $floors;
        $this->materialWall = $materialWall;
        $this->countWindows = $countWindows;
    }

    abstract public function getAddress();
    abstract public function getFloors();
    abstract public function getMaterialWall();
    abstract public function getCountWindows();
}
