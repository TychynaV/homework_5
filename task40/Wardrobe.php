<?php

namespace Task40;

class Wardrobe extends Building
{
    /** @var int  */
    private int $depth; // m.

    /**
     * @param int $depth
     */
    public function setDepth(int $depth): void
    {
        $this->depth = $depth;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return int
     */
    public function getFloors()
    {
        return $this->floors;
    }

    /**
     * @return string
     */
    public function getMaterialWall()
    {
        return $this->materialWall;
    }

    /**
     * @return int
     */
    public function getCountWindows()
    {
        return $this->countWindows;
    }

    /**
     * @return int
     */
    public function getDepth(): int
    {
        return $this->depth;
    }
}
