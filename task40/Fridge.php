<?php

namespace Task40;

class Fridge
{
    use Engineering;

    /** @var string  */
    private string $door;

    /**
     * @param $door
     */
    public function setDoor($door)
    {
        $this->door = $door;
    }

    /**
     *
     */
    public function openDoor()
    {
        switch ($this->door) {
            case 'left':
                echo 'Дверь шириной: '. $this->width.' , высотой '.$this->height. ' открывается в левую сторону.';
                break;
            case 'right':
                echo 'Дверь шириной: '. $this->width.' , высотой '.$this->height. ' открывается в правую сторону.';
                break;
        }
    }

    /**
     * @return int
     */
    public function getVolt(): int
    {
        return $this->volt;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return string
     */
    public function getDoor(): string
    {
        return $this->door;
    }
}
