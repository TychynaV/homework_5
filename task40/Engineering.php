<?php

namespace Task40;

trait Engineering
{
    /** @var string  */
    private string $name;

    /** @var int  */
    private int $width;

    /** @var int  */
    private int $height;

    /** @var int  */
    private int $volt;

    /**
     * Engineering constructor.
     * @param int $width
     * @param int $height
     * @param int $volt
     */
    public function __construct(string $name, int $width, int $height, int $volt)
    {
        $this->name = $name;
        $this->width = $width;
        $this->height = $height;
        $this->volt = $volt;
    }
}
