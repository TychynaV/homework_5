<?php

namespace Task40;

class Microwave extends Fridge
{
    use Engineering;

    /** @var string  */
    private string $grill;

    /**
     * @param $grill
     */
    public function setGrill($grill)
    {
        $this->grill = $grill;
    }

    /**
     * @return string
     */
    public function getGrill(): string
    {
        return $this->grill;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getVolt(): int
    {
        return $this->volt;
    }
}
