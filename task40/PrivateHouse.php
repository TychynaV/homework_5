<?php

namespace Task40;

class PrivateHouse extends Building
{
    /** @var string  */
    private string $furniture;

    /**
     * @param string $furniture
     */
    public function setFurniture(string $furniture): void
    {
        $this->furniture = $furniture;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return int
     */
    public function getFloors()
    {
        return $this->floors;
    }

    /**
     * @return string
     */
    public function getMaterialWall()
    {
        return $this->materialWall;
    }

    /**
     * @return int
     */
    public function getCountWindows()
    {
        return $this->countWindows;
    }

    /**
     * @return string
     */
    public function getFurniture(): string
    {
        return $this->furniture;
    }
}
