<?php

namespace Task40;

class ApartmentHouse extends Building
{
    /** @var int  */
    private int $rooms;

    /**
     * @param int $rooms
     */
    public function setRooms(int $rooms): void
    {
        $this->rooms = $rooms;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return int
     */
    public function getFloors()
    {
        return $this->floors;
    }

    /**
     * @return string
     */
    public function getMaterialWall()
    {
        return $this->materialWall;
    }

    /**
     * @return int
     */
    public function getCountWindows()
    {
        return $this->countWindows;
    }

    /**
     * @return int
     */
    public function getRooms(): int
    {
        return $this->rooms;
    }
}
