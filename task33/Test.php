<?php

namespace Task33;

class Test
{
    use Trait1;
    use Trait2;
    use Trait3;

    public static function getSum()
    {
        return self::getMethod1() + self::getMethod2() + self::getMethod3();
    }
}