<?php

namespace Task25;

class Cube implements CubeInterface
{
    /** @var int  */
    private int $a;

    /**
     * @var int
     */
    private int $b;

    /**
     * @var int
     */
    private int $c;

    /**
     * @var int
     */
    private int $d;

    /**
     * Cube constructor.
     * @param int $a
     * @param int $b
     * @param int $c
     * @param int $d
     */
    public function __construct(int $a, int $b, int $c, int $d)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
        $this->d = $d;
    }

    /**
     * @return float|int
     */
    public function getVolumeCube()
    {
       return $this->a * 3;
    }

    /**
     * @return float|int
     */
    public function getAreaCude()
    {
        return $this->a * 6;
    }
}
