<?php

namespace Task25;

interface CubeInterface
{
    public function __construct(int $a, int $b, int $c, int $d);
    public function getVolumeCube();
    public function getAreaCude();
}
