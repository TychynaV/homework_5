<?php

namespace Task28;

interface Figure
{
    public function getSquare();
    public function getPerimeter();
}
