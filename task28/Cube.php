<?php

namespace Task28;

class Cube implements Figure3d
{
    /** @var int  */
    private int $a;

    /**
     * @var int
     */
    private int $b;

    /**
     * @var int
     */
    private int $c;

    /**
     * @var int
     */
    private int $d;

    /**
     * Cube constructor.
     * @param int $a
     * @param int $b
     * @param int $c
     * @param int $d
     */
    public function __construct(int $a, int $b, int $c, int $d)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
        $this->d = $d;
    }

    /**
     * @return int
     */
    public function getVolume()
    {
        return $this->a * 3;
    }

    /**
     * @return int
     */
    public function getSurfaceSquare()
    {
        return $this->a * 6;
    }

}
