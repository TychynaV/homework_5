<?php

namespace Task28;

interface Figure3d
{
    public function getVolume();
    public function getSurfaceSquare();
}
