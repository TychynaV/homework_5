<?php

namespace Task28;

class Rectangle implements Figure
{
    /** @var int  */
    private int $a;

    /** @var int  */
    private int $b;

    /**
     * Quadrate constructor.
     * @param $a
     * @param $b
     */
    public function __construct(int $a, int $b)
    {
        $this->a = $a;
        $this->b = $b;
    }

    public function getSquare()
    {
        return $this->a * $this->b;
    }

    public function getPerimeter()
    {
        return ($this->a + $this->b) * 2;
    }
}
