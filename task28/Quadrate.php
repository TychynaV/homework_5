<?php

namespace Task28;

class Quadrate implements Figure
{
    /** @var int  */
    private int $a;

    /**
     * Quadrate constructor.
     * @param $a
     */
    public function __construct(int $a)
    {
        $this->a = $a;
    }

    /**
     * @return float
     */
    public function getSquare()
    {
        return $this->a * 2;
    }

    /**
     * @return float
     */
    public function getPerimeter()
    {
        return $this->a * 4;
    }

}
