<?php

namespace Task21;

class Employee extends User
{
    /** @var int  */
    private int $salary;

    public function __construct(string $name, string $surname, $birthday, int $salary)
    {
        parent::__construct($name, $surname, $birthday);
        $this->salary = $salary;
    }

    /**
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }
}