<?php

namespace Task27;

interface UserInterface
{
    public function setName(string $name);
    public function setAge(int $age);
    public function getName();
    public function getAge();
}
