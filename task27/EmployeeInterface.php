<?php

namespace Task27;

interface EmployeeInterface extends UserInterface
{
    public function setSalary(int $salary);
    public function getSalary();
}
