<?php

namespace Task30;

abstract class Figura
{
    protected int $a;

    public function __construct(int $a)
    {
        $this->a = $a;
    }

    abstract public function getSquare();
    abstract public function getPerimeter();
}
