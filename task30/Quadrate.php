<?php

namespace Task30;

class Quadrate extends Figura
{
    /** @var int  */
    private int $b;

    /**
     * Quadrate constructor.
     * @param int $a
     * @param int $b
     */
    public function __construct(int $a, int $b)
    {
        parent::__construct($a);
        $this->b = $b;
    }

    /**
     * @return float|int
     */
    public function getSquare()
    {
        return $this->a * $this->a;
    }

    /**
     * @return float|int
     */
    public function getPerimeter()
    {
        return 4 * $this->a;
    }
}
