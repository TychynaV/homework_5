<?php

namespace Task30;

class Triangle extends Figura
{
    /** @var int  */
    private int $b;

    /** @var int  */
    private int $c;

    public function __construct(int $a, int $b, int $c)
    {
        parent::__construct($a);
        $this->b = $b;
        $this->c = $c;
    }

    public function getSquare()
    {
        return $this->a + $this->b + $this->c;
    }

    public function getPerimeter()
    {
        $P2 = $this->getSquare() / 2;
        return sqrt(($P2 - $this->a) * ($P2 - $this->b) * ($P2 - $this->c) * $P2);
    }
}