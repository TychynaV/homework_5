<?php


namespace Task30;

class Rectangle extends Figura
{
    /** @var int  */
    private int $b;

    /***
     * Rectangle constructor.
     * @param int $a
     * @param int $b
     */
    public function __construct(int $a, int $b)
    {
        parent::__construct($a);
        $this->b = $b;
    }

    public function getSquare()
    {
        return $this->a * $this->b;
    }

    public function getPerimeter()
    {
        return ($this->a + $this->b) * 2;
    }
}
