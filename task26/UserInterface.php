<?php

namespace Task26;

interface UserInterface
{
    public function __construct(string $name, int $age);
    public function getName(): string;
    public function getAge(): int;
}
