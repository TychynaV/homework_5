<?php

namespace Task18;

class Employee
{
    /** @var string  */
    private string $name;

    /** @var string  */
    private string $surname;

    /** @var Post  */
    public Post $post;

    /**
     * Employee constructor.
     * @param string $name
     * @param string $surname
     * @param Post $post
     */
    public function __construct(string $name, string $surname, Post $post)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->post = $post;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getName ()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname ()
    {
        return $this->surname;
    }

    /**
     * @param Post $post
     */
    public function changePost(Post $post)
    {
        $this->post = $post;
    }
}