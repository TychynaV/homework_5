<?php

namespace Task18;

class Post
{
    /** @var string  */
    private string $name;

    /** @var int  */
    private int $salary;

    /**
     * Post constructor.
     * @param string $name
     * @param int $salary
     */
    public function __construct(string $name, int $salary)
    {
        $this->name = $name;
        $this->salary = $salary;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getSalary()
    {
        return $this->salary;
    }
}