<?php

namespace Task29;

interface Tetragon
{
    public function getA();
    public function getB();
    public function getC();
    public function getD();
}
