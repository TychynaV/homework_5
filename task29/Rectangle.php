<?php

namespace Task29;

class Rectangle implements Figure, Tetragon
{
    /** @var int  */
    private int $a;

    /** @var int  */
    private int $b;

    /** @var int  */
    private int $c;

    /**
     * Rectangle constructor.
     * @param int $a
     * @param int $b
     * @param int $c
     */
    public function __construct(int $a, int $b, int $c)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }

    /**
     * @return float|int
     */
    public function getSquare()
    {
        return $this->a * 2;
    }

    /**
     * @return int
     */
    public function getPerimeter()
    {
         return $this->a + $this->b + $this->c;
    }

    public function getA()
    {
        // TODO: Implement getA() method.
    }

    public function getB()
    {
        // TODO: Implement getB() method.
    }

    public function getC()
    {
        // TODO: Implement getC() method.
    }

    public function getD()
    {
        // TODO: Implement getD() method.
    }
}
