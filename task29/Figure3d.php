<?php

namespace Task29;

interface Figure3d
{
    public function getVolume();
    public function getSurfaceSquare();
}
