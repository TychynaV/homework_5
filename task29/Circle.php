<?php

namespace Task29;

interface Circle
{
    public function getRadius();
    public function getDiameter();
}
