<?php

namespace Task29;

interface Figure
{
    public function getSquare();
    public function getPerimeter();
}
