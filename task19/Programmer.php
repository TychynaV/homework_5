<?php

namespace Task19;

class Programmer extends Employee
{
    private string $city;

    /**
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * @param string $say
     */
    public function setSay(string $say)
    {
        parent::setSay($say);
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getSay(): string
    {
        return parent::getSay();
    }
}