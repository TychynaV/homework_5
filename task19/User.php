<?php

namespace Task19;

class User
{
    /** @var string  */
    protected string $name;

    /** @var int  */
    protected int $age;

    /** @var string  */
    protected string $say;

    /**
     * @param int $age
     */
    public function setAge(int $age)
    {
        $this->age = $age;
    }

    /**
     * @param string $say
     */
    protected function setSay(string $say)
    {
        $this->say = $say;
    }

    /**
     * @return string
     */
    protected function getSay()
    {
        return $this->say;
    }

}