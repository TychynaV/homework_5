<?php

namespace Task19;

class Driver extends Employee
{
    /** @var int  */
    private int $experience;

    /** @var string  */
    private string $category;

    /**
     * @param int $experience
     */
    public function setExperience(int $experience)
    {
        $this->experience = $experience;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category)
    {
        $this->category = $category;
    }

    /**
     * @param string $say
     */
    public function setSay(string $say)
    {
        parent::setSay($say);
    }

    /**
     * @return int
     */
    public function getExperience(): int
    {
        return $this->experience;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @return string
     */
    public function getSay()
    {
        return parent::getSay();
    }
}