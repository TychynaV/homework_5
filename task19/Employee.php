<?php

namespace Task19;

class Employee extends User
{
    /** @var int  */
    protected int $salary;

    /**
     * @param int $salary
     */
    public function setSalary(int $salary): void
    {
        $this->salary = $salary;
    }

    /**
     * @param string $say
     */
    public function setSay(string $say)
    {
        parent::setSay($say);
    }

    /**
     * @return string
     */
    public function getSay()
    {
        return parent::getSay();
    }
}