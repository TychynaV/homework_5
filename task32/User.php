<?php

namespace Task32;

class User
{
    use Helper;

    /**
     * User constructor.
     * @param string $name
     * @param int $age
     */
    public function __construct(string $name, int $age)
    {
        $this->name = $name;
        $this->age = $age;
    }
}
