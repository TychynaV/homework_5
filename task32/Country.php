<?php

namespace Task32;

class Country
{
    /** @var int  */
    private int $population;

    use Helper;

    /**
     * Country constructor.
     * @param string $name
     * @param int $age
     * @param int $population
     */
    public function __construct(string $name, int $age, int $population)
    {
        $this->name=$name;
        $this->age=$age;
        $this->population=$population;
    }

    /**
     * @return int
     */
    public function getPopulation(): int
    {
        return $this->population;
    }
}
