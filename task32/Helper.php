<?php

namespace Task32;

trait Helper
{
    /** @var string  */
    private string $name;

    /** @var int  */
    private int $age;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }
}