<?php

namespace Task22;

class ArraySumHelper
{
    /**
     * @param array $array
     * @param int $number
     * @return array|float|int
     */
    public static function ArraySumHelper(array $array, int $number)
    {
        $arr = [];
        foreach ($array as $k => $v) {
            $arr[] = $v * $number;
        }
        return $arr;
    }
}