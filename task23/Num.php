<?php

namespace Task23;

class Num
{
    /** @var int  */
    public static int $num1;

    /** @var int  */
    public static int $num2;

    /** @var int  */
    private static int $num3 = 3;

    /** @var int  */
    private static int $num4 = 5;

    /** @var int  */
    protected const GET_VIEW = 10;

    /** @var int  */
    public const DIRECTLY = 12;

    /**
     * @return int
     */
    public function getSum()
    {
        return self::$num3 + self::$num1;
    }

    /**
     * @return int
     */
    public static function getConst()
    {
        return self::GET_VIEW;
    }
}
