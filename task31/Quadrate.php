<?php

namespace Task31;

class Quadrate implements Figura
{
    /**
     * @var int
     */
    private int $a;

    /**
     * Quadrate constructor.
     * @param int $a
     */
    public function __construct(int $a)
    {
        $this->a = $a;
    }

    /**
     * @return float|int
     */
    public function getSquare()
    {
        return $this->a * $this->a;
    }

    /**
     * @return float|int
     */
    public function getPerimeter()
    {
        return 4 * $this->a;
    }
}
