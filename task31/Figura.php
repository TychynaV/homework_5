<?php

namespace Task31;

interface Figura
{
    public function getSquare();
    public function getPerimeter();
}