<?php

namespace Task31;

class Rectangle implements Figura
{
    /**
     * @var int
     */
    private int $a;

    /**
     * @var int
     */
    private int $b;

    /**
     * Rectangle constructor.
     * @param int $a
     * @param int $b
     */
    public function __construct(int $a, int $b)
    {
        $this->a = $a;
        $this->b = $b;
    }

    /**
     * @return float|int
     */
    public function getSquare()
    {
        return $this->a * $this->b;
    }

    /**
     * @return float|int
     */
    public function getPerimeter()
    {
        return ($this->a + $this->b) * 2;
    }
}
