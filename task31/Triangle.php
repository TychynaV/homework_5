<?php

namespace Task31;

class Triangle implements Figura
{
    /** @var int  */
    private int $a;

    /** @var int  */
    private int $b;

    /** @var int  */
    private int $c;

    /**
     * Triangle constructor.
     * @param int $a
     * @param int $b
     * @param int $c
     */
    public function __construct(int $a, int $b, int $c)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }

    /**
     * @return int
     */
    public function getSquare()
    {
        return $this->a + $this->b + $this->c;
    }

    /**
     * @return float
     */
    public function getPerimeter()
    {
        $P2 = $this->getSquare() / 2;
        return sqrt(($P2 - $this->a) * ($P2 - $this->b) * ($P2 - $this->c) * $P2);
    }
}
