<?php

namespace Task31;

class FiguresCollection
{
    /** @var array  */
    private array $array = [];

    /**
     * @param Figura $data
     */
    public function addFigure(Figura $data)
    {
        $this->array[] = $data;
    }

    /**
     * @return int
     */
    public function getTotalSquare()
    {
        $sum = 0;
        foreach ($this->array as $k) {
            $sum += $k->getSquare();
        }
        return $sum;
    }
}