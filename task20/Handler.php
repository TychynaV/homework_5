<?php

namespace Task20;

class Handler extends Student
{
    /** @var string  */
    private string $surname;

    /**
     * @param $name
     * @return bool
     */
    protected function validName($name): bool
    {
        return parent::validName($name);
    }
}
