<?php

namespace Task20;

use InvalidArgumentException;

class Student
{
    /** @var string  */
    public string $name;

    /** @var string  */
    public string $surname;

    /** @var int  */
    public int $course;

    /** @var string  */
    protected string $courseAdministrator;

    /**
     * @param string $name
     * @param string $value
     * @return string
     */
    public function __set(string $name, string $value)
    {
        return $this->$name = $value;
    }

    /**
     * @param string $name
     */
    public function setCourseAdministrator(string $name): void
    {
            if($this->validName($name)) {
                $this->courseAdministrator = $name;
            }else {
                throw new InvalidArgumentException("Invalid name");
            }
    }

    /**
     * @return string
     */
    public function getCourseAdministrator()
    {
        return $this->courseAdministrator;
    }

    /**
     * @return string
     */
    public function __getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @param int $course
     */
    public function transferToNextCourse(string $name, int $course)
    {
        if($this->isCourseCorrect($course)) {
            $this->course = $course;
        }
    }

    /**
     * @param $data
     * @return bool
     */
    private function isCourseCorrect($data): bool
    {
        return $this->course>$data || $data <= 5;
    }

    /**
     * @param $name
     * @return bool
     */
    protected function validName($name): bool
    {
        $name = explode(" ", $name);
        for ($i=0; count($name); $i++)
        {
            return (mb_strlen($name[$i]) > 2) && (mb_strlen($name[$i]) < 20);
        }
    }
}
